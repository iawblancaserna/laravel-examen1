@extends('layouts.header')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <h1 class="text-primary"> Welcome to my web!</h1>
        </div>
        <div class="row">
            <input class="btn btn-primary btn-lg" type="button" value="List Products" id="listProducts" onclick="location.href='/products'">
        </div>
    </div>
@endsection